$(document).ready(function(){
	
	$('#popup').hide();
	
	$('.read-more-1').slideUp(0);
	$('.read-more-2').slideUp(0);
	var gate = true;
	var slideGate = true;
	var hamburgerGate = true;


	

	$('#mail-us, .ion-close-round').click(function(e){
		
		$(".overlay").toggle();

		if(gate){
		
			$('#popup').fadeIn(500);

			$('h3').css({'border':'7px solid rgba(46,195,183,0.1)'});
			
			// $('#services-image-1').css({

			// 	'filter': 'grayscale(5%)',
			// 	'-webkit-filter': 'grayscale(5%)'
				
			// })

			$('img').css({
				'filter': 'opacity(20%)'
			});
			$('.service-boxes').css({
				'filter': 'opacity(5%)'
			})
			$('.blur-it').css({
				'color' : 'rgba(255,255,255,0.1)'
			})

			$('html, body').css({
			    overflow: 'hidden',
			    height: '100%'
			});

			gate=!gate;

		} else{

			$('#popup').fadeOut(500);

			$('h3').css({'border':'7px solid rgba(46,195,183,1)'});

			$('img').css({
				'filter': 'opacity(100%)'
			});
			$('.service-boxes').css({
				'filter': 'opacity(100%)'
			})
			$('.blur-it').css({
				'color' : 'rgba(255,255,255,1)'
			})

			$('html, body').css({
			    overflow: 'auto',
			    height: 'auto'
			});
			gate=!gate;
		}


		
	});

	$('.about-us-link').click(function(){
		$('html,body').animate({
			scrollTop: $('.at-summit-edge').offset().top - 70,
			easing: 'easeOutBounce'
		}, 500);
	});

	$('.services-link').click(function(){
		$('html,body').animate({
			scrollTop: $('.our-services').offset().top - 70,
			easing: 'easeOutBounce'
		}, 500);
	});

	$('.contact-us-link').click(function(){
		$('html,body').animate({
			scrollTop: $('.footer').offset().top - 70,
			easing: 'easeOutBounce'
		}, 500);
		$('#contact-us-link').toggle();
	});

	$('.expand-1').click(function(e){
		e.preventDefault();
		
		if(slideGate){
			$('.read-more-1').slideDown(200);
			$('.expand-1').html("Read less");
			slideGate = !slideGate;
		} else {
			$('.read-more-1').slideUp(200);
			$('.expand-1').html("Read more");
			slideGate = !slideGate;
		}
	});

	$('.expand-2').click(function(e){
		e.preventDefault();
		
		if(slideGate){
			$('.read-more-2').slideDown(200);
			$('.expand-2').html("Read less");
			slideGate = !slideGate;
		} else {
			$('.read-more-2').slideUp(200);
			$('.expand-2').html("Read more");
			slideGate = !slideGate;
		}
	});

	$('.dropdown-wrapper').slideUp(0);

	$('.hamburger').click(function(){

		if(hamburgerGate){

			$('.dropdown-wrapper').slideDown(200);
			$('.hamburger > i').css({

				'color': 'rgba(0,0,0,0.7)'
			})

			hamburgerGate = !hamburgerGate;
		}else{
			$('.dropdown-wrapper').slideUp(200);
			$('.hamburger > i').css({

				'color': '#fff'
			})
			hamburgerGate = !hamburgerGate;
		}

		$('.dropdown-wrapper > div > a').click(function(){
			$('.dropdown-wrapper').slideUp(200);
			$('.hamburger > i').css({

				'color': '#fff'
			})
			hamburgerGate = !hamburgerGate;
		});
	})	

});